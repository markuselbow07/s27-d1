let http = require("http");

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"

	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];
http.createServer(function(request, response){
	// GET request rom mock DB
	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}
	// POT request to the mock DB
	if(request.url == "/users" && request.method == "POST"){
		let requestBody = ' ';
		// stream -  sequence of data
		request.on('data', function(data){

			requestBody += data;
		});

		// response and step - only runs after the requet has completely been sent
		request.on('end', function(){
			// Checks the data type or requestBody
			console.log(typeof requestBody);
			// Convert the string requestBody to JSON
			requestBody = JSON.parse(requestBody);			
				// Create a new object representing the new mock database record
				let newUser = {
					"name" : requestBody.name,
					"email" : requestBody.email
				}

				// Add the new user to the database
				directory.push(newUser);
				console.log(directory);

				response.writeHead(200, {'Content-Type': 'application/json'});
				response.write(JSON.stringify(newUser));
				response.end();
		});
	}
}).listen(4000);

console.log('Server is running at localhost:4000');
